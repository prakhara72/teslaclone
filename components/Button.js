import React from "react";
import { StyleSheet, Text, View, Pressable } from "react-native";

export default function StyledButton(props){
    const {type,content}=props;
    const backgroundcolor=type==='primary'?'#171A20CC':'#FFFFFFA6';
    const textcolor=type==='primary'?'#FFFFFF':'#171A20';

    return (
        <View style={styles.container}>
            <Pressable 
            style={[styles.button,{backgroundColor:backgroundcolor}]}
            >
                <Text style={[styles.text,{color:textcolor}]}>{content}</Text>
            </Pressable>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        width:'100%',
        paddingTop:10,
        paddingBottom:10,
        paddingLeft:20,
        paddingRight:20,
    },
    button:{
        // backgroundColor:'grey',
        height:40,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
    },
    text:{
        color:'white',
        fontSize:12,
        fontWeight:'500',
        textTransform:'uppercase',
    }
})