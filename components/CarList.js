import React from 'react';
import { StyleSheet, Text, View, Image, FlatList, Dimensions } from 'react-native';
import cars from './cars';
import CarItem from './CarItem';

export default function CarList(){

    return (
        <View>
            <FlatList
                data={cars}
                renderItem={({item})=><CarItem car={item} />}
                showsVerticalScrollIndicator={false}
                snapToAlignment={'start'}
                decelerationRate={'fast'}
                snapToInterval={Dimensions.get('window').height}
            />
        </View>
    );
}
