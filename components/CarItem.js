import React from "react";
import { StyleSheet, Text, View, ImageBackground,Dimensions } from "react-native";
import StyledButton from "./Button";

export default function CarItem(props) {
  const {name,tagline,image,taglineCTA}=props.car;
  return (
    <View style={styles.carContainer}>
      <ImageBackground
        source={image}
        style={styles.image}
      />
      <View style={styles.titles}>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.subtitle}>
          {tagline}
          {' '}
          <Text style={styles.subCTA}>
            {taglineCTA}
          </Text>
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <StyledButton
          type="primary"
          content={"Custom Order"}
        />
        <StyledButton type="secondary" content={"Existing Inventory"} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  carContainer: {
    width: "100%",
    height: Dimensions.get('window').height,
  },
  titles: {
    marginTop: "30%",
    width: "100%",
    alignItems: "center",
  },
  title: {
    fontSize: 40,
    fontWeight: "600",
  },
  subtitle: {
    fontSize: 16,
    color: "#5c5e62",
  },
  subCTA:{
    textDecorationLine:'underline',
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
    position: "absolute",
  },
  buttonContainer:{
    position:'absolute',
    bottom:40,
    width:'100%',
  }
});
