import React from "react";
import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Header from "./components/Header";
import CarList from "./components/CarList";

export default function App() {
  return (
    <View style={styles.container}>
      <Header />
      <CarList />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop:32,
    flex: 1,
    backgroundColor: "#fff",
    alignContent:'center',
    justifyContent:'center',
  },
});
